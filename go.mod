module gitlab.com/flotfeali/uptimer

go 1.12

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	golang.org/x/tools/gopls v0.4.1 // indirect
	gopkg.in/alexcesaro/quotedprintable.v2 v2.0.0-20150314193201-9b4a113f96b3 // indirect
	gopkg.in/gomail.v1 v1.0.0-20150320132819-11b919ab4933
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
