package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"

	"gitlab.com/flotfeali/uptimer/monitor/data"
)

// TrackAPI has all routes related to track data manipulation
type TrackAPI struct {
	data *data.DataMonitor
}

// Start a new instance of track api
func (api *TrackAPI) Start(data *data.DataMonitor) {
	api.data = data
}

// ListHandler handles GET request returning all tracks
func (api *TrackAPI) ListHandler(rw http.ResponseWriter, req *http.Request) {
	query := req.URL.Query()
	targetID := ""
	if len(query["targetId"]) > 0 {
		targetID = query["targetId"][0]
	}

	offset := 0
	if len(query["page"]) > 0 {
		if num, err := strconv.Atoi(query["page"][0]); err == nil {
			offset = num
		}
	}

	j, err := json.Marshal(api.data.Track.Find(offset, targetID))
	if err != nil {
		panic(err)
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(j)
}

// DetailHandler handles GET request returning a single target
func (api *TrackAPI) DetailHandler(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	j, err := json.Marshal(api.data.Track.FindOneByID(vars["id"]))
	if err != nil {
		panic(err)
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(j)
}
