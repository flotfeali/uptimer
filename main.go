package main

import (
	"gitlab.com/flotfeali/uptimer/monitor"
	"gitlab.com/flotfeali/uptimer/server"
	"log"
	"net/http"
	"os"
	"time"

	dm "gitlab.com/flotfeali/uptimer/monitor/data"
)

var (
	db        = dm.DB{}
	data      = dm.DataMonitor{}
	job       = monitor.Job{}
	router    = server.Router{}
	websocket = server.Websocket{}
)

func main() {
	db.Start()
	defer db.Close()

	data.Start(db)

	http.Handle("/", router.Start(&data))
	http.HandleFunc("/ws", websocket.Start(&data))

	job.Start(&data, getTimeTargetsVerification(), getTimeLive())

	addr := getServiceAddress()
	log.Printf("Server running on http://%s", addr)
	if err := http.ListenAndServe("0.0.0.0"+addr, nil); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

	time.AfterFunc
}

func getTimeTargetsVerification() string {
	if env := os.Getenv("CHECK_TARGETS_AT_EVERY"); env != "" {
		return env
	}

	return "1m"
}

func getTimeLive() string {
	if env := os.Getenv("CHECK_LIVE_AT_EVERY"); env != "" {
		return env
	}

	return "30s"
}

func getServiceAddress() string {
	if env := os.Getenv("PORT_BEHIND_PROXY"); env != "" {
		return ":" + env
	}
	if env := os.Getenv("VIRTUAL_PORT"); env != "" {
		return ":" + env
	}

	return ":3000"
}
