package data

import (
	"log"
	"time"

	"gitlab.com/flotfeali/uptimer/monitor/entities"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// DataTrack is the data configuration related to Track collection
type DataTrack struct {
	collection *mgo.Collection
	events     chan entities.Event
}

// Find all tracks
func (d *DataTrack) Find(offset int, targetID string) []entities.Track {
	tracks := []entities.Track{}

	query := bson.M{}
	if targetID != "" {
		query["targetId"] = bson.ObjectIdHex(targetID)
	}

	if offset < 0 {
		offset = offset * 50
	}

	err := d.collection.Find(query).Limit(50).Sort("-createdAt").All(&tracks)
	if err != nil {
		log.Printf("got an error finding a doc %v\n", err)
	}

	return tracks
}

// FindOneByID finds a single target by the id field
func (d *DataTrack) FindOneByID(id string) *entities.Track {
	_id := bson.ObjectIdHex(id)
	var target entities.Track

	err := d.collection.Find(bson.M{"_id": _id}).One(&target)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}

		log.Printf("got an error finding a doc %v\n", err)
	}

	return &target
}

// Create a new track
func (d *DataTrack) Create(target entities.Target, response interface{}, status int) *entities.Track {
	doc := entities.Track{
		ID:        bson.NewObjectId(),
		TargetID:  target.ID,
		Status:    status,
		Response:  response,
		CreatedAt: time.Now(),
	}
	if err := d.collection.Insert(doc); err != nil {
		log.Printf("Can't insert document: %v\n", err)
	}

	return &doc
}

// RemoveByTargetID removes a track by the targetId field
func (d *DataTrack) RemoveByTargetID(id string) {
	err := d.collection.Remove(bson.M{"targetId": bson.ObjectIdHex(id)})
	if err != nil {
		log.Printf("Can't delete document: %v\n", err)
	}
}

// Start a new instance of data track
func (d *DataTrack) Start(db DB, events chan entities.Event) {
	d.collection = db.Session.DB(db.DBName).C("track")
	d.events = events
}
