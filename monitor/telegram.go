package monitor

import (
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/flotfeali/uptimer/monitor/data"
	"gitlab.com/flotfeali/uptimer/monitor/entities"
)

var (
	bot tgbotapi.BotAPI
)

func init() {
	b, err := tgbotapi.NewBotAPI(os.Getenv("BOT_KEY"))
	if err != nil {
		log.Fatalln(err.Error())
	}
	bot = *b

	go chatIDHandler()
}

// sendMessage takes a chatID and sends "polo" to them
func sendMessage(target entities.Target, response map[string]interface{}, id bson.ObjectId) {
	chatIds := target.Bots
	message := "Hi,\nThe target <b>" + target.URL + "</b> is back UP (HTTP <b>" + strconv.Itoa(target.Status) + "</b>). "

	if target.Status < 500 {
		message += "☺️"
	} else {
		message += "😔"
	}

	if msg, ok := response["message"]; ok {
		message += "\n<b>message:</b>\n" + msg.(string)
	}

	message += "\n<b>id:</b> <a href='" + os.Getenv("BASE_URL") + id.Hex() + "' >" + id.Hex() + "</a>"

	if len(target.Tags) > 0 {
		message += "\n"
		for _, tag := range target.Tags {
			message += tag + " "
		}
	}

	for _, s := range chatIds {
		msg := tgbotapi.NewMessageToChannel(s, message)
		msg.ParseMode = "HTML"
		if _, err := bot.Send(msg); err != nil {
			log.Println(err.Error())
		}
	}
}

func broadcast(targetData *data.DataTarget) {
	msg := tgbotapi.NewMessageToChannel("@uptimer_test", "Hi \nI'm still alive. Easy to get to work. I check all the services. 😌")
	msg.ParseMode = "HTML"
	if _, err := bot.Send(msg); err != nil {
		log.Println(err.Error())
	}
}

func chatIDHandler() {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, _ := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "your chatId is: <b>"+strconv.Itoa(int(update.Message.Chat.ID))+"</b> \n for get notify off uptime service add this to your target bots")
		msg.ReplyToMessageID = update.Message.MessageID
		msg.ParseMode = "HTML"
		if _, err := bot.Send(msg); err != nil {
			log.Println(err.Error())
		}
	}
}
