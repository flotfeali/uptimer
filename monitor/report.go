package monitor

import (
	"gitlab.com/flotfeali/uptimer/monitor/data"
	"fmt"
	"time"
)

const intervalPeriod time.Duration = 24 * time.Hour

const hourToTick int = 7
const minuteToTick int = 0
const secondToTick int = 0

// ReportTicker start report on day
type ReportTicker struct {
	data         	 *data.DataMonitor
	timer 			 *time.Timer
}

func getNextTickDuration() time.Duration {
	now := time.Now()
	nextTick := time.Date(now.Year(), now.Month(), now.Day(), hourToTick, minuteToTick, secondToTick, 0, time.Local)
	if nextTick.Before(now) {
		nextTick = nextTick.Add(intervalPeriod)
		fmt.Println("Next Tick is :"+nextTick.Format(time.RFC1123))
	}
	return nextTick.Sub(time.Now())
}

func (jt ReportTicker) updateJobTicker() {
	fmt.Println("next tick here")
	jt.timer.Reset(getNextTickDuration())
}

func (jt ReportTicker) handle(){
	for{
		<- jt.timer.C
		jt.sendReport()
		jt.updateJobTicker()
	}
}


func (jt ReportTicker) sendReport(){
	targets := jt.data.Target.GetAll()

	for _,traget := range targets {
		go 
	}
}

func(jt ReportTicker) start(data *data.DataMonitor){
	jt.timer = time.NewTimer(getNextTickDuration())
	jt.data  = data
	jt.handle()
}
