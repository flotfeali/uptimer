package monitor

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/flotfeali/uptimer/monitor/data"
)

// Job is responsible for running the polling request for all targets
type Job struct {
	data         *data.DataMonitor
	checkAtEvery time.Duration
	liveAtEvery  time.Duration
}

func (j Job) checkTargetsStatus() {
	results := AsyncHTTPGets(j.data.Target.GetAllURLS())
	for _, result := range results {
		status := http.StatusBadGateway
		response := new(map[string]interface{})
		if result.Response != nil {
			log.Printf("%s status: %s\n", result.URL, result.Response.Status)
			status = result.Response.StatusCode
			resp, _ := ioutil.ReadAll(result.Response.Body)
			_ = json.Unmarshal([]byte(resp), &response)
		}
		j.saveTracking(result.URL, status, *response)
	}
}

func (j Job) saveTracking(url string, status int, response map[string]interface{}) {
	target := j.data.Target.FindOneByURL(url)
	oldStatus := target.Status
	track := j.data.Track.Create(*target, response, status)
	target.Status = status
	j.data.Target.Update(target.ID.Hex(), *target)

	// sends notification only if the status has changed
	if oldStatus != status {
		//SendNotificaton(*target)  disable send email
		sendMessage(*target, response, track.ID)
	}
}

func (j Job) checkTargetsPeriodically() {

	ticker := time.NewTicker(j.checkAtEvery)
	liveTicker := time.NewTicker(j.liveAtEvery)
	go func() {
		for {
			select {
			case <-ticker.C:
				log.Printf("Checking %d URLs status...", len(j.data.Target.GetAll()))
				j.checkTargetsStatus()

			}
		}
	}()

	go func() {
		for {
			select {
			case <-liveTicker.C:
				broadcast(j.data.Target)
			}
		}
	}()
}

// Start a new instance of Job
func (j Job) Start(data *data.DataMonitor, checkAtEvery string, liveAtEvery string) {
	j.data = data

	duration, err := time.ParseDuration(checkAtEvery)
	if err != nil {
		log.Fatalf("Value %v is not a valid duration time", checkAtEvery)
	}
	j.checkAtEvery = duration

	duration, err = time.ParseDuration(liveAtEvery)
	if err != nil {
		log.Fatalf("Value %v is not a valid duration time", checkAtEvery)
	}
	j.liveAtEvery = duration

	log.Printf("Starting targets checking async (every %s)", checkAtEvery)
	j.checkTargetsPeriodically()
}
