package entities

import "gopkg.in/mgo.v2/bson"

// Target data structure
type Target struct {
	ID     bson.ObjectId `bson:"_id" json:"id,omitempty"`
	URL    string        `bson:"url" json:"url,omitempty"`
	Status int           `bson:"status" json:"status,omitempty"`
	Emails []string      `bson:"emails" json:"emails,omitempty"`
	Bots   []string      `bson:"bots" json:"bots,omitempty"`
	Tags   []string      `bson:"tags" json:"tags,omitempty"`
}
